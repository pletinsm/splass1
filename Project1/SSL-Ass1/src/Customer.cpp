#include "..\include\Customer.h"
#include <fstream>
#include <iostream>
#include <string>


class Customer {
public:
	Customer(std::string c_name, int c_id) : name(c_name), id(c_id) { customerIndex++; };
	virtual std::vector<int> order(const std::vector<Dish> &menu) = 0;
	virtual std::string toString() const = 0;
	std::string getName() const { return this->name; }
	int getId() const { return this->id; };
private:
	const std::string name;
	const int id;
	int customerIndex = 0;
protected:
	void changeIndex() { customerIndex = customerIndex + 1; }

};
//------------------------------------veg
class VegetarianCustomer : public Customer {
public:
	VegetarianCustomer(std::string name, int id) : Customer(name, id) {};
	std::vector<int> order(const std::vector<Dish> &menu) {
		std::vector<int> ans;
		bool isOrderd = false;
		Dish *expBVG = NULL;
		//Fixed-->why 2 loops // its make thing easier
		for each (Dish d in menu)
		{
			if (d.getType() == VEG & !(isOrderd)) {//fixed -->if there is more than 1 loop the ans can contain several different VEG dishes
				ans.push_back(d.getId());//Yes, according to my understanding -->is this under the assumption that the first dish in menu is witht he smallest id? 
				isOrderd = true;
			}

			if (expBVG == NULL & d.getType == BVG)
				expBVG = &d;
			
			if (expBVG == NULL & d.getType() == BVG)
				if (d.getPrice() > (*expBVG).getPrice())
					expBVG = &d;
				
		}
		ans.push_back((*expBVG).getId());
		return ans;
	};
	std::string toString() const {
		return this->getName() + "," + std::to_string(this->getId()) + " is veg";
	};
private:
};

//------------------------------------cheap

class CheapCustomer : public Customer {
public:
	CheapCustomer(std::string name, int id) : Customer(name, id) {};
	std::vector<int> order(const std::vector<Dish> &menu) {
		std::vector<int> ans;
		Dish *cheapestDish = NULL;
		if (!(this->isOrdered)) {
			//fixed-->again why 2 for loops?
			for each (Dish d in menu)
			{
				if(cheapestDish == NULL)
					cheapestDish = &d;

				if (cheapestDish == NULL && d.getPrice() < (*cheapestDish).getPrice())//key Board mistake-->why take thew highest price?
					cheapestDish = &d;                        
			}
			ans.push_back((*cheapestDish).getId());
			this->isOrdered = true;	//addition
		}
		return ans;
		
	};
	std::string toString() const 
	{
		return this->getName() + "," + std::to_string(this->getId()) + " is cheap";
	};
private:
	bool isOrdered = false;
};

//------------------------------------Spicy

class SpicyCustomer : public Customer {
public:
	SpicyCustomer(std::string name, int id) : Customer(name, id) {};
	std::vector<int> order(const std::vector<Dish> &menu) {
		std::vector<int> ans;
		Dish *expSPC = NULL;
		Dish *cheapBVG = NULL;
		//the assignment says that the first order should be SPC,
		//and the NEXT orders should be BVG(one each time for each call of SpicyCustomer::order(..))
		// ^
		// |
		//fixed
		if (isFirst) {
			for each (Dish d in menu)
			{
				if (expSPC == NULL & d.getType() == SPC)
					expSPC = &d;
				
				if (expSPC != NULL & d.getType() == SPC)
					if(d.getPrice > (*expSPC).getPrice())
						expSPC = &d;

			}
			this->isFirst = false;
			ans.push_back((*expSPC).getId());
		}
		else {
			for each (Dish d in menu)
			{
				if (cheapBVG == NULL & d.getType() == BVG)
					cheapBVG = &d;

				if (cheapBVG != NULL & d.getType() == BVG)
					if (d.getPrice() < (*cheapBVG).getPrice())
						cheapBVG = &d;
			}
		}
		return ans;
	};
	std::string toString() const {
		return this->getName() + "," + std::to_string(this->getId()) + " is spicy";
	};
private:
	bool isFirst = true;
};

//------------------------------------Alcoholic

class AlchoholicCustomer : public Customer {
public:
	AlchoholicCustomer(std::string name, int id) : Customer(name, id) {};
	std::vector<int> order(const std::vector<Dish> &menu) {
		std::vector<int> ans;
		Dish *cheapALC;
		//consider change: to store only the price+id of current order and each following order() take the next more expensive ALC dish in menu
		//thus there is no need for store a whole vector as member and fiction contains.(more efficient)
		// ^
		// |
		// fixed
		for each (Dish d in menu)
		{
			if (cheapALC == NULL & d.getType() == ALC)
				cheapALC = &d;

			if (cheapALC != NULL & d.getType() == ALC)
				if (d.getPrice() < (*cheapALC).getPrice() & d.getPrice() >= this->lastPrice
						& d.getId != this->lastId)
					cheapALC = &d;
		}
		ans.push_back((*cheapALC).getId());
		//orderdBefore.push_back((*cheapALC).getId());
		this->lastPrice = (*cheapALC).getPrice;
		this->lastId = (*cheapALC).getId;
		return ans;
	};
	std::string toString() const {
		return this->getName() + "," + std::to_string(this->getId()) + " is alcoholic";
	};
private:
	/*std::vector<int> orderdBefore;
	bool contains(std::vector<int> v, Dish dish) {
		for each (int d in v)
		{
			if (d == dish.getId())
				return false;

			return true;
		}
	}; */
	int lastPrice;
	int lastId;
};