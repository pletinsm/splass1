#include "..\include\Table.h"
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>


class Table {
public:
	Table(int t_capacity) : capacity(t_capacity) {
		this->open = false;
		this->customersList = std::vector<Customer*>(capacity);
	};
	int getCapacity() const { return this->capacity; };
	void addCustomer(Customer* customer) {
		if (this->capacity = this->customersList.size())
			std::cout << "This table is full";
		else
			this->customersList.push_back(customer);
	};
	void addCustomer(Customer* customer, std::vector<OrderPair> ol) {
		if (this->capacity = this->customersList.size())
			std::cout << "This table is full";
		else {
			this->customersList.push_back(customer);
			for each (OrderPair op in ol)
			{
				this->orderList.push_back(op);
			}
		}
	};
	void removeCustomer(int id) {
		if (this->customersList.size == 0)
			std::cout << "the customer is not in this table";
		
		else
		{
			std::vector<OrderPair> ordersToRemove;
			for each (Customer *c in this->customersList)
			{
				if (c->getId() == id)
				{
					ordersToRemove = this->getCustomerOrders(id);
					//TODO: safe delete of pointer
					this->customersList.erase(std::remove(this->customersList.begin(), this->customersList.end(), *c), this->customersList.end());

				}
			}
			for each (OrderPair toRemove in ordersToRemove)
			{
				for each (OrderPair op in this->orderList)
				{
					if (toRemove == op)
					{
						this->orderList.erase(std::remove(this->orderList.begin(), this->orderList.end(), op), this->orderList.end());
					}
				}
			}
		}

	};
	Customer* getCustomer(int id) {
		Customer *ans = nullptr;
		if (this->customersList.size == 0)
			std::cout << "the customer is not in this table";

		else
		{			
			for each (Customer *c in this->customersList)
			{
				if (c->getId() == id)
				{
					ans = c;
				}
			}

			if(ans == nullptr)
				std::cout << "the customer is not in this table";
		}

		return ans;
	};
	std::vector<Customer*>& getCustomers() {
		return this->customersList;
	};
	std::vector<OrderPair>& getOrders() {
		return this->orderList;
	};
	void order(const std::vector<Dish> &menu) {
		std::vector<int> cOrder;
		std::vector<Dish> cOrderDishes;
		for each (Customer *c in this->customersList)
		{
			cOrder = c->order(menu);//take order
			for (int  i = 0; i < cOrder.size-1; i++)
			{
				for each (Dish d in menu)
				{
					if (d.getId() == cOrder[i])//find every dish from the order in the menu
					{
						this->orderList.push_back(OrderPair(c->getId(), d));
						std::cout << c->getName() + " ordered " + d.getName();
					}
				}
			}
		}
	};
	void openTable() {
		this->open = true;
	};
	void closeTable() {
		this->open = false;
		for each (Customer *c in this->customersList)
		{
			//TODO: Check delete
			delete c;
			this->customersList.erase(std::remove(this->customersList.begin(), this->customersList.end(), *c), this->customersList.end());
		}
	};
	int getBill() {
		int sum = 0;
		for each (OrderPair op in this->orderList)
		{
			sum = sum + op.second.getPrice();
		}
		return sum;
	};
	 bool isOpen() { return this->isOpen; };
	 int getFreeSeats() { return this->capacity - this->customersList.size(); };
	 std::vector<OrderPair> getCustomerOrders(int cId) {
		 std::vector<OrderPair> ans;
		 for each (OrderPair op in this->orderList)
		 {
			 if (op.first == cId)
			 {
				 ans.push_back(op);
			 }
		 }
		 
	 }

	//Rule of 5
	//1
	virtual ~Table() {
		for each (Customer *c in this->customersList)
		{
			delete c;
			this->customersList.erase(std::remove(this->customersList.begin(), this->customersList.end(), *c), this->customersList.end());
		}
		for each (OrderPair op in this->orderList)
		{
			this->orderList.erase(std::remove(this->orderList.begin(), this->orderList.end(), op), this->orderList.end());
		}
	};
	//2
	Table(const Table &other) {
		this->capacity = other.getCapacity();
		this->open = other.open; 
		this->orderList = other.orderList;
		for each (Customer *c in other.customersList)
		{
			this->customersList.push_back(CustomerFactory::createCustomer(c->getType(), c->getName(), c->getId()));
		}
	};
	//3
	Table &Table::operator=(const Table &other) {
		if (this == &other)
			return *this;

		this->capacity = other.getCapacity();
		this->open = other.open;
		this->orderList = other.orderList;

		this->customersList.clear();
		for each (Customer *c in other.customersList)
		{
			this->customersList.push_back(CustomerFactory::createCustomer(c->getType(), c->getName(), c->getId()));
		}


		return *this;

	};
	//4
	Table(Table &&other): capacity(other.capacity) {
		this->open = other.open;
		this->orderList = other.orderList;//check
		this->customersList.clear();
		for each (Customer *c in other.customersList)
		{
			this->customersList.push_back(c);
			c = nullptr;
		}
	}
	//5
	Table &Table::operator=(Table &&other) {
		if (this != &other) {
			this->orderList.clear();
			this->customersList.clear();

			this->capacity = other.getCapacity();
			this->open = other.open;
			this->orderList = other.orderList;
			for each (Customer *c in other.customersList)
			{
				this->customersList.push_back(c);
				c = nullptr;
			}

			return *this;
		}
	};
private:
	int capacity;
	bool open;
	std::vector<Customer*> customersList;
	std::vector<OrderPair> orderList; //A list of pairs for each order in a table - (customer_id, Dish)
};
