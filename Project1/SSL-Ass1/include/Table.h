#ifndef TABLE_H_
#define TABLE_H_

#include <vector>
#include "Customer.h"
#include "Dish.h"

typedef std::pair<int, Dish> OrderPair;

class Table{
public:
    Table(int t_capacity);
	virtual ~Table();//1
	Table(const Table &other);//2
	Table &Table::operator=(const Table &other);//3
    int getCapacity() const;
    void addCustomer(Customer* customer);
	void addCustomer(Customer* customer, std::vector<OrderPair> orders);
    void removeCustomer(int id);
    Customer* getCustomer(int id);
    std::vector<Customer*>& getCustomers();
    std::vector<OrderPair>& getOrders();
    void order(const std::vector<Dish> &menu);
    void openTable();
    void closeTable();
    int getBill();
    bool isOpen();
	int getFreeSeats();
	std::vector<OrderPair> getCustomerOrders(int cId);
private:
    int capacity;
    bool open;
    std::vector<Customer*> customersList;
    std::vector<OrderPair> orderList; //A list of pairs for each order in a table - (customer_id, Dish)
};


#endif