#include "..\include\Dish.h"
#include <fstream>
#include <iostream>
#include <string>

#include "..\include\Restaurant.h"

enum DishType {
	VEG, SPC, BVG, ALC
};

class Dish {
public:
	Dish(int d_id, std::string d_name, int d_price, DishType d_type) :id(d_id), name(d_name), price(d_price), type(d_type) {};
	int getId() const { return this->id; };
	std::string getName() const { return this->name; };
	int getPrice() const { return this->price; };
	DishType getType() const { return this->type; };
	static DishType getDishTypeByName(std::string name) {};
private:
	const int id;
	const std::string name;
	const int price;
	const DishType type;
};