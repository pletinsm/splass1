

#include "..\include\Restaurant.h"
#include <fstream>
#include <iostream>
#include <string>

class Restaurant{
public:
	Restaurant() {

	}
	Restaurant(const std::string &configFilePath) {
		int tables_size;
		
		std::ifstream configFile(configFilePath);
		if (configFile.is_open())
		{
			std::string line;
			std::getline(configFile,line);
			skipLines(configFile, line);

			// create tables vector
			tables_size = std::atoi(line.c_str());
			tables = std::vector<Table*>(tables_size); // check if we need to use new
			skipLines(configFile, line);

			// fill tables vector
			std::string * tablesArr = new std::string[tables_size]();
			splitByComma(line, tablesArr, tables_size);
			std::vector<Table*>::iterator tableIter = tables.begin();
			for (int i = 0; i < tables_size; i++)
				tableIter = tables.insert(tableIter,new Table(std::atoi(tablesArr[i].c_str())));
			delete tablesArr;
			skipLines(configFile, line);

			//fill Menu
			std::string * menuDishEntryArr = new std::string[MENU_ENRTY_SIZE]();
			menu = std::vector<Dish>(); // check if we need to use new
			int i = 0;
			while (line != "" || line != "\n" || line != "\r\n" || line[0] != '#') {
				//std::vector<Dish>::iterator dishIter = menu.begin();
				splitByComma(line, menuDishEntryArr, MENU_ENRTY_SIZE);
				menu.push_back(Dish(i, menuDishEntryArr[0], std::atoi(menuDishEntryArr[2].c_str()), Dish::getDishTypeByName( menuDishEntryArr[1])));
				skipLines(configFile, line);
			}

			skipLines(configFile, line);
		}

		else std::cout << "Error in Copy constructor";// todo error message 

	}


	void start(){
		std::cout << "Restaurant is now open!" << std::endl;
		open = true;
	}
	int getNumOfTables() const {
		return tables.size;
	}
	Table* getTable(int ind) { //pointer
		if (ind >= getNumOfTables())
			;//  todo print error or something 
		return tables[ind];
	}
	const std::vector<BaseAction*>& getActionsLog() const { // Return a reference to the history of actions
		return  actionsLog;
	}

	std::vector<Dish>& getMenu() {
		return menu;
	}
	//Rule of 5
	//1
	virtual ~Restaurant() {
		for each (Table *t in this->tables)
		{
			delete t;
		}
		for each (BaseAction *ba in this->actionsLog)
		{
			delete ba;
		}
	};
	//2
	Restaurant(const Restaurant &other) {
		this->open = other.open;
		this->menu = other.menu;

		for each (Table *t in other.tables)
		{
			this->tables.push_back(new Table(t->getCapacity()));
		}
		for each (BaseAction *ba in other.actionsLog)
		{
			this->actionsLog.push_back(ActionFactory::copyAction(ba));
		}
		
	};
	//3
	Restaurant &Restaurant::operator=(const Restaurant &other) {
		if (this == &other)
			return *this;

		this->open = other.open;
		this->menu = other.menu;

		this->tables.clear();
		this->actionsLog.clear();

		for each (Table *t in other.tables)
		{
			this->tables.push_back(new Table(t->getCapacity()));
		}
		for each (BaseAction *ba in other.actionsLog)
		{
			this->actionsLog.push_back(ActionFactory::copyAction(ba));
		}

		return *this;

	};
	//4
	Restaurant(Restaurant &&other){

		this->open = other.open;
		this->menu = other.menu;

		this->tables.clear();
		this->actionsLog.clear();

		for each (Table *t in other.tables)
		{
			this->tables.push_back(t);
			t = nullptr;
		}
		for each (BaseAction *ba in other.actionsLog)
		{
			this->actionsLog.push_back(ba);
			ba = nullptr;
		}

	}
	//5
	Restaurant &Restaurant::operator=(Restaurant &&other) {
		if (this != &other)
		{
			this->tables.clear();
			this->actionsLog.clear();

			this->open = other.open;
			this->menu = other.menu;

			for each (Table *t in other.tables)
			{
				this->tables.push_back(t);
				t = nullptr;
			}
			for each (BaseAction *ba in other.actionsLog)
			{
				this->actionsLog.push_back(ba);
				ba = nullptr;
			}

			return *this;
		}
	};

private:
	void skipLines(std::ifstream& stream, std::string& line) {
		while (line == "" || line == "\n" || line == "\r\n" || line[0] == '#')//todo chech if line could be without characters
			std::getline(stream, line);
	}

	void splitByComma(std::string line,std::string  arr[] ,int size) {
		int count = 0;
		for (int i = 0; i < size && line.length != 0; i++) {
			int index = line.find_first_of(',');
			arr[i] = line.substr(0, index);		// take first comma seperated word
			line = line.substr(index);			// cut the section that we already scanned
			count++;							// count inserted items
		}
		if (count != size)
			;// error --------
	}
	//----------------------------------------------------------fields
	bool open;
	std::vector<Table*> tables;
	std::vector<Dish> menu;
	std::vector<BaseAction*> actionsLog;
	const int MENU_ENRTY_SIZE = 3;
};
